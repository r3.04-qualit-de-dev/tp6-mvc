package main;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Interface extends Application implements Observer{
    Label res1;
    Label res2;
    Label res3;
    Dice de1;
    Dice de2;
    Dice de3;

    public void update(Subject subj) {
        res1.setText(de1.getRes() + "");
    }

    public void update(Subject subj, Object data) {
        if(((int) data)==12) res1.setText(de1.getRes() + "");
        if(((int) data)==50) res2.setText(de2.getRes() + "");
        if(((int) data)==100) res3.setText(de3.getRes() + "");
    }

    public void start(Stage stage) {
        de1 = new Dice(12,this);
        de2 = new Dice(50,this);
        de3 = new Dice(100,this);
        Button button1 = new Button(de1.toString());
        Button button2 = new Button(de2.toString());
        Button button3 = new Button(de3.toString());
        Button all = new Button("Lancer dés");
        res1 = new Label("Caca");
        res2 = new Label("Chiasse");
        res3 = new Label("Coulante");

        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                de1.lancer_dice();
            }
        });

        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                de2.lancer_dice();
            }
        });

        button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                de3.lancer_dice();
            }
        });
        
        all.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                de1.lancer_dice();
                de2.lancer_dice();
                de3.lancer_dice();
            }
        });

;
        VBox vb1 = new VBox(button1, res1);
        VBox vb2 = new VBox(button2, res2);
        VBox vb3 = new VBox(button3, res3);
        HBox hb = new HBox(vb1,vb2,vb3);
        VBox tout = new VBox(hb,all);
        Scene scene = new Scene(tout, 640, 480);
        stage.setScene(scene);
        stage.show();

        System.out.println("go");
    }

    public static void main(String[] args) {
        launch();
    }

}
