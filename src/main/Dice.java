package main;
public class Dice extends Subject{
    private int nb_face;
    private int res = 0;

    public Dice(int nb_face, Observer obs){
        this.nb_face = nb_face;
        attach(obs);
    }

    public void lancer_dice(){
        this.res = (int) (Math.random()*(this.nb_face));
        this.notifyObservers(nb_face);
        }

    public String toString(){
        return "lancer dé " + this.nb_face;
    }

    public int getRes(){
        return this.res;
    }

}